/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 *
 * @description SmartComponent for employees
 */

import { connect } from 'react-redux';

import { loadEmployees, searchEmployee, deleteEmployee } from '../../store/actions/employee';

import Employees from './Employees';

/**
 * @description simple object, which will be combined in the props of the component
 * @param {*} state The store state.
 */
const mapStateToProps = state => ({
  employees: state.employees
});

const mapDispatchToProps = {
  loadEmployees,
  searchEmployee,
  deleteEmployee
};

const EmployeesContainer = connect(mapStateToProps, mapDispatchToProps)(Employees);
export default EmployeesContainer;
