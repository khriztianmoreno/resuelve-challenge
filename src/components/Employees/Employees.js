/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import EmployeeItem from '../EmployeeItem/EmployeeItem';

/**
 * @description Basic component for the management of employees
 */
class Employees extends PureComponent {
  constructor() {
    super();

    this.state = {
      employees: [],
      currency: false,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    const { loadEmployees } = this.props;

    loadEmployees();
  }

  // React 16
  static getDerivedStateFromProps(props) {
    return {
      employees: props.employees
    };
  }

  handleInputChange({ target }) {
    const search = target.value;
    const { searchEmployee } = this.props;

    searchEmployee(search);
  }

  handleDelete(id) {
    const { deleteEmployee } = this.props;

    deleteEmployee(id);
  }

  render() {
    const { employees, currency } = this.state;
    const { history } = this.props;

    return (
      <React.Fragment>
        <button type="button" onClick={() => { history.push('/employee/create'); }} className="btn btn-outline">
          Add Employee
        </button>
        <button type="button" onClick={() => { console.table(employees); }} className="btn btn-outline">
          Print
        </button>
        <button
          type="button"
          onClick={() => { this.setState(prevState => ({ currency: !prevState.currency })); }}
          className="btn btn-outline"
        >
          Change Currency
        </button>
        <div className="form-group">
          <input
            name="search"
            type="text"
            placeholder="Search ..."
            className="form-control"
            onChange={this.handleInputChange}
          />
        </div>
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>
                #
              </th>
              <th>
                Name
              </th>
              <th>
                Company
              </th>
              <th>
                Salary
              </th>
              <th>
                Age
              </th>
              <th>
                Phone
              </th>
              <th>
                Email
              </th>
              <th>
                --
              </th>
            </tr>
          </thead>
          <tbody>
            {
              employees && employees.map(item => (
                <tr key={item.id}>
                  <EmployeeItem
                    employee={item}
                    currency={currency}
                    onDelete={this.handleDelete}
                  />
                </tr>))
            }
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

/**
 * Typechecking props
 */
Employees.propTypes = {
  loadEmployees: PropTypes.func.isRequired,
  searchEmployee: PropTypes.func.isRequired,
  deleteEmployee: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default withRouter(Employees);
