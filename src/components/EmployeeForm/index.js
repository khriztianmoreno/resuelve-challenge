/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class EmployeeForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleInputChange({ target }) {
    this.setState({
      [target.name]: target.value
    });
  }

  handleOnSubmit(evt) {
    evt.preventDefault();

    const { onSubmit, employee } = this.props;
    const {
      name,
      company,
      salary,
      age,
      phone,
      email
    } = this.state;


    const data = {
      ...employee,
      name: name || employee.name,
      company: company || employee.company,
      salary: Number(salary) || Number(employee.salary),
      age: Number(age) || Number(employee.age),
      phone: phone || employee.phone,
      email: email || employee.email,
    };

    onSubmit(data);
  }

  render() {
    const { employee, edit } = this.props;

    return (
      <form onSubmit={this.handleOnSubmit}>
        <div className="form-group">
          <label htmlFor="name">
            Name
            <input
              name="name"
              type="text"
              className="form-control"
              placeholder="Enter you full name"
              onChange={this.handleInputChange}
              defaultValue={employee && employee.name}
              required
            />
          </label>
        </div>
        <div className="form-group">
          <label htmlFor="company">
            Company
            <input
              name="company"
              type="text"
              className="form-control"
              placeholder="Enter company name"
              disabled={edit}
              onChange={this.handleInputChange}
              defaultValue={employee && employee.company}
              required
            />
          </label>
        </div>
        <div className="form-group">
          <label htmlFor="salary">
            Salary
            <input
              name="salary"
              type="number"
              className="form-control"
              placeholder="Enter salary"
              onChange={this.handleInputChange}
              defaultValue={employee && employee.salary}
              required
            />
          </label>
        </div>
        <div className="form-group">
          <label htmlFor="age">
            Age
            <input
              name="age"
              type="number"
              className="form-control"
              placeholder="Enter age"
              onChange={this.handleInputChange}
              defaultValue={employee && employee.age}
              required
            />
          </label>
        </div>
        <div className="form-group">
          <label htmlFor="phone">
            Phone
            <input
              name="phone"
              type="text"
              className="form-control"
              placeholder="Enter phone"
              onChange={this.handleInputChange}
              defaultValue={employee && employee.phone}
              required
            />
          </label>
        </div>
        <div className="form-group">
          <label htmlFor="email">
            Email
            <input
              name="email"
              type="email"
              placeholder="Enter email"
              className="form-control"
              onChange={this.handleInputChange}
              defaultValue={employee && employee.email}
              required
            />
          </label>
        </div>
        <button type="submit" className="btn btn-outline">
          Submit
        </button>
      </form>
    );
  }
}

/**
 * Typechecking props
 */
EmployeeForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  employee: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    company: PropTypes.string,
    salary: PropTypes.number,
    age: PropTypes.number,
    phone: PropTypes.string,
    email: PropTypes.string,
  }),
  edit: PropTypes.bool,
};


EmployeeForm.defaultProps = {
  employee: {},
  edit: false
};

export default EmployeeForm;
