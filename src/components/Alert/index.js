/**
 * @author Cristian Moreno
 */

import React from 'react';
import PropTypes from 'prop-types';

const Message = ({ msg, type }) => (
  <div className={`alert alert-${type}`}>
    {msg}
  </div>
);


/**
 * Typechecking props
 */
Message.propTypes = {
  msg: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['done', 'error']).isRequired,
};

export default Message;
