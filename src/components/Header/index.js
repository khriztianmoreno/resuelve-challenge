/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React from 'react';
import PropTypes from 'prop-types';

const Header = ({ title }) => (
  <header className="page-header">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h2>
            {title}
          </h2>
        </div>
      </div>
    </div>
  </header>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
