/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const AMOUN_LIMIT = 10000;
const VALUE_USD = 21.50;

const EmployeeItem = ({ employee, currency, onDelete }) => {
  const amountClasses = employee.salary > AMOUN_LIMIT ? 'amount amount-high' : 'amount amount-low';
  const formatCurrency = currency ? 'USD' : 'MXN';

  let { salary } = employee;
  if (formatCurrency === 'USD') {
    salary = Number(salary) / VALUE_USD;
  }

  const editLink = `/employee/${employee.id}`;

  return (
    <React.Fragment key={employee.id}>
      <td>
        <Link to={editLink}>
          {employee.id}
        </Link>
      </td>
      <td>
        <Link to={editLink}>
          {employee.name}
        </Link>
      </td>
      <td>
        {employee.company}
      </td>
      <td className={amountClasses}>
        {salary.toLocaleString('en-IN', { style: 'currency', currency: formatCurrency })}
      </td>
      <td className="amount">
        {employee.age}
      </td>
      <td>
        {employee.phone}
      </td>
      <td>
        {employee.email}
      </td>
      <td>
        <button type="button" onClick={() => { onDelete(employee.id); }}>
          <img src="/img/delete.png" alt="delete employee" />
        </button>
      </td>
    </React.Fragment>
  );
};

EmployeeItem.propTypes = {
  employee: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    company: PropTypes.string,
    salary: PropTypes.number,
    age: PropTypes.number,
    phone: PropTypes.string,
    email: PropTypes.string,
  }).isRequired,
  currency: PropTypes.bool.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default EmployeeItem;
