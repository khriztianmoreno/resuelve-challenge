/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import {
  ADD_EMPLOYEE,
  LOAD_EMPLOYEES,
  SEARCH_EMPLOYEE,
  DELETE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  FIND_EMPLOYEE
} from '../types/employee';

import employees from '../../employees';

const initialState = {
  employees,
  saved: false
};


export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_EMPLOYEE: {
      const id = state.employees.length + 1;
      const employee = { id, ...action.employee };
      return {
        ...state,
        employees: state.employees.concat(employee),
        saved: true
      };
    }
    case SEARCH_EMPLOYEE: {
      const { search } = action;
      const cloneEmployees = state.cloneEmployees || state.employees;

      return {
        ...state,
        cloneEmployees,
        employees: cloneEmployees.filter(employee => employee.name
          .toLocaleLowerCase().includes(search)
          || employee.company.toLocaleLowerCase().includes(search))
      };
    }
    case DELETE_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.filter(employeee => employeee.id !== action.id),
      };
    case FIND_EMPLOYEE:
      return {
        ...state,
        employee: state.employees.find(employeee => employeee.id === Number(action.id)),
      };
    case UPDATE_EMPLOYEE: {
      const employeesChanged = state.employees.map((employee) => {
        if (employee.id === Number(action.id)) {
          return {
            ...employee,
            name: action.data.name,
            salary: Number(action.data.salary),
            age: Number(action.data.age),
            phone: action.data.phone,
            email: action.data.email,
          };
        }
        return employee;
      });
      return {
        ...state,
        employees: employeesChanged,
        saved: true
      };
    }
    case LOAD_EMPLOYEES:
      return {
        ...state,
        employee: undefined,
        saved: false,
      };
    default:
      return state;
  }
};
