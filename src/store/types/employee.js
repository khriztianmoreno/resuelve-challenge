/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

export const ADD_EMPLOYEE = 'ADD_EMPLOYEE';
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE';
export const LOAD_EMPLOYEES = 'LOAD_EMPLOYEES';
export const SEARCH_EMPLOYEE = 'SEARCH_EMPLOYEE';
export const FIND_EMPLOYEE = 'FIND_EMPLOYEE';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';
