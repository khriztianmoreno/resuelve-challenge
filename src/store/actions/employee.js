/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import {
  ADD_EMPLOYEE,
  LOAD_EMPLOYEES,
  SEARCH_EMPLOYEE,
  DELETE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  FIND_EMPLOYEE
} from '../types/employee';

export function loadEmployees() {
  return {
    type: LOAD_EMPLOYEES,
  };
}

export function createEmployee(employee) {
  return {
    type: ADD_EMPLOYEE,
    employee,
  };
}

export function searchEmployee(search) {
  return {
    type: SEARCH_EMPLOYEE,
    search,
  };
}

export function deleteEmployee(id) {
  return {
    type: DELETE_EMPLOYEE,
    id,
  };
}

export function editEmployee(id, data) {
  return {
    type: UPDATE_EMPLOYEE,
    id,
    data,
  };
}

export function findEmployee(id) {
  return {
    type: FIND_EMPLOYEE,
    id,
  };
}
