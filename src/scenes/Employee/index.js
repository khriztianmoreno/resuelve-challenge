/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import ListEmployee from './List';
import CreateEmployee from './Create';
import EditEmployee from './Edit';

export { ListEmployee, CreateEmployee, EditEmployee };
