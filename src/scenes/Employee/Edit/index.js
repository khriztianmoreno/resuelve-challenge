/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 *
 * @description SmartComponent for Create employee
 */

import { connect } from 'react-redux';

import { findEmployee, editEmployee } from '../../../store/actions/employee';

import Edit from './Edit';

/**
 * @description simple object, which will be combined in the props of the component
 * @param {*} state The store state.
 */
const mapStateToProps = state => ({
  employee: state.employee,
  saved: state.saved
});

const mapDispatchToProps = {
  findEmployee,
  editEmployee
};

const EditEmployeeContainer = connect(mapStateToProps, mapDispatchToProps)(Edit);
export default EditEmployeeContainer;
