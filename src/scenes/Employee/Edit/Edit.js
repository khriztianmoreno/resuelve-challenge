/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import EmployeeForm from '../../../components/EmployeeForm';
import Header from '../../../components/Header';
import Message from '../../../components/Alert';

class EditEmployee extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      employee: {},
    };

    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  // React 16
  static getDerivedStateFromProps(props) {
    const { match: { params }, findEmployee } = props;

    if (props.saved) {
      return {
        saved: props.saved
      };
    }

    if (props.employee) {
      return {
        employee: props.employee
      };
    }

    if (params.id) {
      findEmployee(params.id);
      return {
        id: params.id,
      };
    }

    return {};
  }

  handleOnSubmit(employee) {
    const { editEmployee } = this.props;

    editEmployee(employee.id, employee);
  }

  render() {
    const { employee, saved } = this.state;

    return (
      <React.Fragment>
        <Header title="Edit Employee" />
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Link to="/employee">
                Go back
              </Link>
            </div>
            <div className="col-md-12">
              { saved && <Message msg="Updated employee" type="done" /> }
            </div>
            <div className="col-md-12">
              <EmployeeForm
                onSubmit={this.handleOnSubmit}
                employee={employee}
                edit
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/**
 * Typechecking props
 */
EditEmployee.propTypes = {
  editEmployee: PropTypes.func.isRequired,
};

export default EditEmployee;
