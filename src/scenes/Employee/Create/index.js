/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 *
 * @description SmartComponent for Create employee
 */

import { connect } from 'react-redux';

import { createEmployee } from '../../../store/actions/employee';

import Create from './Create';

/**
 * @description simple object, which will be combined in the props of the component
 * @param {*} state The store state.
 */
const mapStateToProps = state => ({
  saved: state.saved
});

const mapDispatchToProps = {
  createEmployee
};

const CreateContainer = connect(mapStateToProps, mapDispatchToProps)(Create);
export default CreateContainer;
