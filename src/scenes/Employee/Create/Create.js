/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import EmployeeForm from '../../../components/EmployeeForm';
import Header from '../../../components/Header';
import Message from '../../../components/Alert';

class CreateEmployee extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  // React 16
  static getDerivedStateFromProps(props) {
    if (props.saved) {
      return {
        saved: props.saved
      };
    }
    return {};
  }

  handleOnSubmit(employee) {
    const { createEmployee } = this.props;
    const newEmployee = { ...employee, salary: Number(employee.salary), age: Number(employee.age) };
    createEmployee(newEmployee);
  }

  render() {
    const { saved } = this.state;

    return (
      <React.Fragment>
        <Header title="Create Employee" />
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Link to="/employee">
                Go back
              </Link>
            </div>
            <div className="col-md-12">
              { saved && <Message msg="New employee created" type="done" /> }
            </div>
            <div className="col-md-12">
              <EmployeeForm onSubmit={this.handleOnSubmit} />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

/**
 * Typechecking props
 */
CreateEmployee.propTypes = {
  createEmployee: PropTypes.func.isRequired,
};

export default CreateEmployee;
