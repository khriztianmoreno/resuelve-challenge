import React from 'react';

import Employees from '../../../components/Employees';
import Header from '../../../components/Header';

/* eslint-disable */
const ListEmployee = () => (
  <React.Fragment>
    <Header title="List Employees"/>
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <Employees />
        </div>
      </div>
    </div>
  </React.Fragment>
);

export default ListEmployee;
