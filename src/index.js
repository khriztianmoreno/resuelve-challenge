import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Routes from './routes';
import './main.css';

import employeeReducers from './store/reducers/employee';

/* eslint-disable */
const store = createStore(
  employeeReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
/* eslint-enable */

const Root = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);

ReactDOM.render(<Root />, document.getElementById('root'));
