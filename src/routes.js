import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from './App';
import { ListEmployee, CreateEmployee, EditEmployee } from './scenes/Employee';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route
        path="/"
        exact
        component={App}
      />
      <Route
        path="/employee"
        exact
        component={ListEmployee}
      />
      <Route
        path="/employee/create"
        exact
        component={CreateEmployee}
      />
      <Route
        path="/employee/:id"
        exact
        component={EditEmployee}
      />
    </Switch>
  </BrowserRouter>
);

export default Routes;
